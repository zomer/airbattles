﻿using UnityEngine;
using System.Collections;
using UnityEngine.UI;

[System.Serializable]
public class Level 
{
	public Button.ButtonClickedEvent OnClickEven;
	public string LevelText;
}
