﻿using UnityEngine;
using System.Collections;
using System.Collections.Generic;
using UnityEngine.UI;

public class LevelManager : MonoBehaviour 
{
	public GameObject gameManager;
	public GameObject button;
	public Transform levelGrid;
	public List<Level> levels;
	private const string LEVEL_NAME_PREFIX = "Level_";



	private void Start () 
	{
		FilleLevels();
	}

    private void FilleLevels () 
	{
		foreach (var level in levels) 
		{
			GameObject newButton = Instantiate(button) as GameObject;
			Button levelButton  = newButton.GetComponent<Button> ();
			levelButton.GetComponentInChildren<Text>().text = level.LevelText;
			levelButton.transform.SetParent (levelGrid);
			levelButton.onClick.AddListener(() => { 
				GameManager.LoadLevel(LEVEL_NAME_PREFIX + level.LevelText);
			});

		}
	}
}