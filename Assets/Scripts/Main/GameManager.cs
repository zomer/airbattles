﻿using UnityEngine;
using System.Collections;

public class GameManager : MonoBehaviour 
{
	private static GameManager _instance;
	public static State GameState;

	public enum State
	{
		StartMenu,
		LevelMenu,
		PlayGame
	}


	private void Awake ()
	{
		InitGameMananger ();
	}

	public void LoadLevelMenu () 
	{
		SetState (State.StartMenu);
		Application.LoadLevel ("LevelMenu");
	}

	public static void LoadLevel (string title) 
	{
		SetState (State.PlayGame);
		Application.LoadLevel (title);
	}

	public static GameManager getInstance()
	{
		return _instance;
	}

	private static void SetState (State state) {
		GameState = state;
		UpdateState ();
	}

	private void InitGameMananger () 
	{
		if (_instance == null) 
		{
			_instance = this;
			DontDestroyOnLoad(gameObject);
		} else if (_instance != this) 
		{
			Destroy(gameObject);    
		}
	}

	private static void UpdateState () 
	{
		switch (GameState) {
		case State.StartMenu:
			break;
		case State.LevelMenu:
			break;
		case State.PlayGame:
			break;
		}

	}
}
