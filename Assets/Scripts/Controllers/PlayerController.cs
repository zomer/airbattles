﻿using UnityEngine;
using System.Collections;
using UnityEngine.EventSystems;


public class PlayerController : MonoBehaviour 
{
	public float speedValue;
	private Vector2 _speed;
	private Vector2 _targetPosition;
	private Vector2 _relativePosition;
	private Vector2 _movement;



	private void Start() 
	{
		_speed = new Vector2(speedValue, speedValue);
	}

	private void Update()
	{
	
		if (Input.GetMouseButtonDown(0))
		{  
			if (!EventSystem.current.IsPointerOverGameObject())
			{
				_targetPosition = Camera.main.ScreenToWorldPoint(Input.mousePosition);
			}
		}
		
		_relativePosition = new Vector2(
			_targetPosition.x - gameObject.transform.position.x,
			_targetPosition.y - gameObject.transform.position.y );
	}
	
	private void FixedUpdate()
	{
		if (_speed.x * Time.deltaTime >=  Mathf.Abs(_relativePosition.x)) 
		{
			_movement.x = _relativePosition.x;   
		} else {
			_movement.x = _speed.x * Mathf.Sign(_relativePosition.x);
		}
		
		if (_speed.y * Time.deltaTime >=  Mathf.Abs(_relativePosition.y)) {
			_movement.y = _relativePosition.y;
		} else {
			_movement.y = _speed.y * Mathf.Sign(_relativePosition.y);
		}
		
		GetComponent<Rigidbody2D>().velocity = _movement;
	}

	public void Shoot () 
	{
		WeaponScript weaponScript = GetComponent<WeaponScript> ();
		if (weaponScript != null) 
		{
			weaponScript.Attack(false);
		}
	}
}
