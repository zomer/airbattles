﻿using UnityEngine;
using System.Collections;

public class EnemyController : MonoBehaviour 
{
	private WeaponScript _weaponScript;



	private void Start () 
	{
		_weaponScript = GetComponent<WeaponScript> ();
	}
	
	private void Update () 
	{
		if (_weaponScript != null) 
		{
			_weaponScript.Attack(true);
		}

		DestroyGameObjectInEndArea ();
	}

	private void DestroyGameObjectInEndArea () 
	{
		Vector2 min = Camera.main.ViewportToWorldPoint (new Vector2 (0, 0));
		if (transform.position.x < min.x) 
		{
			Destroy(gameObject);
		}
	}
}
