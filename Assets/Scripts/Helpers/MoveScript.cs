﻿using UnityEngine;
using System.Collections;

public class MoveScript : MonoBehaviour 
{
	public float speedValue;
	public Vector2 direction = new Vector2(-1, 0);

	private Vector2 _movement;
	private Vector2 _speed;
	private Rigidbody2D _rigidbodyComponent;



	private void Start () 
	{
		_speed = new Vector2 (speedValue, speedValue);
	} 

	private void Update()
	{
		_movement = new Vector2(
			_speed.x * direction.x,
			_speed.y * direction.y);

		if (_rigidbodyComponent == null) {
			_rigidbodyComponent = GetComponent<Rigidbody2D>();
		}
		
		_rigidbodyComponent.velocity = _movement;
	}
}
