﻿using UnityEngine;
using System.Collections;
using UnityEngine.UI;

public class HealthScript : MonoBehaviour 
{
	public GameObject prefabExplosion;
	public Image imageLive;
	public Image imageDie;
	public int hp = 1;
	public bool isEnemy = true;
	public const int DEFAULT_DAMAGE = 1;



	private void OnTriggerEnter2D (Collider2D otherCollider)
	{
		ShotScript shot = otherCollider.gameObject.GetComponent<ShotScript>();
		if (shot != null)
		{
			if (shot.isEnemyShot != isEnemy)
			{
				Damage(shot.damage);
				shot.Disable ();
			}
		}

		EnemyController enemy = otherCollider.gameObject.GetComponent<EnemyController>();
		if (enemy != null) 
		{
			Damage (DEFAULT_DAMAGE);
			Destroy (enemy);
		}
	}

	private void Damage (int damageCount)
	{
		hp -= damageCount;
		
		if (hp <= 0)
		{
			DestroerExplosion ();
			Destroy (gameObject);
		}
	}

	private void DestroerExplosion() {
		GameObject explosion = Instantiate (prefabExplosion);
		explosion.transform.position = transform.position;
	}
}
