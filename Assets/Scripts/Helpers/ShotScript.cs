﻿using UnityEngine;
using System.Collections;

public class ShotScript : MonoBehaviour 
{
	public int damage = 1;
	public bool isEnemyShot = false;
	private const float LIFE_TIME = 5f;



	private void OnEnable () 
	{
		Invoke ("Disable", LIFE_TIME);
	}

	public void Disable () 
	{
		gameObject.SetActive (false);
	}

	private void OnDisable () 
	{
		CancelInvoke ();
	}
}
