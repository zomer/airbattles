﻿using UnityEngine;
using System.Collections;

public class EnemiesSpawner : MonoBehaviour 
{
	public GameObject enemy;
	public float spawnTime = 2f; 


	private void Start () 
	{
		InvokeRepeating ("SpawnEnemy", spawnTime, spawnTime);
	}

	private void Update () 
	{

	}

	void SpawnEnemy ()
	{
		Vector2 min = Camera.main.ViewportToWorldPoint (new Vector2 (1, 0)); 
		Vector2 max = Camera.main.ViewportToWorldPoint (new Vector2 (1, 2));

		GameObject anaEnemy = Instantiate (enemy);
		enemy.transform.position = new Vector2 (max.x, Random.Range(min.y , max.y));
	}
}
