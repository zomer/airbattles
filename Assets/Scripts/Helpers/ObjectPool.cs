﻿using UnityEngine;
using System.Collections;
using System.Collections.Generic;

public class ObjectPool : MonoBehaviour 
{
	public static int poolSize = 10;
	public static Dictionary<GameObject, List<GameObject>> objectsPools = new Dictionary<GameObject, List<GameObject>> ();
	

	public static GameObject GetPoolObject (GameObject prefab) {

		if (!objectsPools.ContainsKey(prefab)) 
		{
			CreateObjectPool(prefab, poolSize);

			return GetPoolObject(prefab);
		}

		var pool = objectsPools [prefab];

		for (int i = 0; i < pool.Count; i++) 
		{
			if (!pool[i].activeInHierarchy) 
			{
				return pool[i];
			}
		}

		return null;
	}

	public static void CreateObjectPool (GameObject prefab, int count) 
	{
		List<GameObject> objects = new List<GameObject> ();

		for (int i = 0; i < count; i++) 
		{
			GameObject instance = Instantiate<GameObject>(prefab);
			instance.SetActive(false);
			objects.Add(instance);
		}

		objectsPools.Add (prefab, objects);
	}
}
