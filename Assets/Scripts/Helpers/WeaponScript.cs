﻿using UnityEngine;
using System.Collections;

public class WeaponScript : MonoBehaviour 
{
	public GameObject bulletPosition;
	public float shootingRate = 0.25f;
	public GameObject shotPrefab;
	private float _shootCooldown;



	private void Start()
	{
		_shootCooldown = 0f;
	}
	
	private void Update()
	{
		if (_shootCooldown > 0)
		{
			_shootCooldown -= Time.deltaTime;
		}
	}

	public void Attack(bool isEnemy)
	{
		if (CanAttack)
		{
			_shootCooldown = shootingRate;
			
			GameObject bullet = ObjectPool.GetPoolObject(shotPrefab);
			if (bullet != null) {
				bullet.SetActive(true);

				var shotTransform = bullet.transform;

				SetBulletStartPosition(shotTransform);

				ShotScript shot = shotTransform.gameObject.GetComponent<ShotScript>();
				if (shot != null)
				{
					shot.isEnemyShot = isEnemy;
				}
			}
		}
	}
	
	public bool CanAttack
	{
		get
		{
			return _shootCooldown <= 0f;
		}
	}

	private void SetBulletStartPosition (Transform shotTransform) 
	{
		if (bulletPosition != null) {
			shotTransform.position = bulletPosition.transform.position;
		} else {
			shotTransform.position = transform.position;
		}
	}
}
